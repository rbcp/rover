
function pp (o) {
    for (var k in o) {
        print(k+": "+o[k]);
    }
}


/*
 * Utils: versions
 */

function version_string_parse (s) {
    var sp = s.split(".");
    for (var i = 0, n = sp.length; i < n; ++i) {
        sp[i] = parseInt(sp[i]);
    }
    return sp;
}

function version_compare (a, b) {
    var an = a.length;
    var bn = b.length;
    for (var i = 0; i < Math.min(an, bn); ++i) {
        var at = a[i];
        var bt = b[i];
        if (at > bt) {
            return -1;
        }
        if (bt > at) {
            return 1;
        }
    }
    if (an > bn) {
        return -1;
    }
    if (bn > an) {
        return 1;
    }
    return 0;
}


/*
 * Utils: Math
 */
var tau = Math.PI * 2;
var tau4 = Math.PI / 2;
var tau24 = Math.PI / 12;

function degrees_to_radians (d) {
    return Math.PI * (d / 180.0);
}

function radians_to_degrees (r) {
    return r / Math.PI * 180.0;
}

/**
 * travel_spherical takes a spherical point ({az,el,dis}), a bearing,
 * and a distance, and returns the spherical destination point.
 * p1.dis is used as the spherical radius.
 */
function travel_spherical (p1, bearing, d) {
    var unit_d = d / p1.dis;
    var dest_el = Math.asin(Math.sin(p1.el) * Math.cos(unit_d) +
                            Math.cos(p1.el) * Math.sin(unit_d) * Math.cos(bearing));
    var dest_az = p1.az + Math.atan2(Math.sin(bearing) * Math.sin(unit_d) * Math.cos(p1.el),
                                     Math.cos(unit_d) - Math.sin(p1.el) * Math.sin(dest_el));
    return { az: dest_az, el: dest_el, dis: p1.dis };
}


function gcd (a, b, tol) {
    if (tol == null) {
        tol = 0;
    }
    a = Math.abs(a);
    b = Math.abs(b);
    if (b > a) {
        var temp = a;
        a = b;
        b = temp;
    }
    while (true) {
        if (b <= tol) {
            return a;
        }
        a %= b;
        if (a <= tol) {
            return b;
        }
        b %= a;
    }
}


/*
 * Utils: files
 */

function drop_filename (pathname) {
    var slash = pathname.lastIndexOf('/');
    var backslash = pathname.lastIndexOf('\\');
    return pathname.substr(0, Math.max(slash, backslash)+1);
}

function drop_path_component (pathname) {
    var sp = pathname.split(/[\\/]+(?=[^\\/])/);
    sp.pop();
    return sp.join("/");
}

function take_path_component (pathname) {
    var sp = pathname.split(/[\/\\]+(?=[^\\/])/);
    var c = sp.pop();
    var slash = c.lastIndexOf('/');
    var backslash = c.lastIndexOf('\\');
    var sb = Math.max(slash, backslash);
    if (sb > -1) {
        return c.substr(0, sb);
    } else {
        return c;
    }
}

function take_filename (pathname) {
    var slash = pathname.lastIndexOf('/');
    var backslash = pathname.lastIndexOf('\\');
    var filename = pathname.substr(Math.max(slash, backslash)+1);
    return filename;
}

function take_basename (pathname) {
    var filename = take_filename(pathname);
    var dot = filename.lastIndexOf('.');
    var basename = filename.substr(0, dot);
    return basename;
}

function join_paths (a, b) {
    var alast = a[a.length-1];
    var bfirst = b[0] || "";
    if (bfirst == ".") {
        var bsp = b.split(/[\/\\]/);
        var adrop = 0;
        var bskip = 0;
        for (var i = 0, n = bsp.length; i < n; ++i) {
            if (bsp[i] == ".") {
                bskip++;
            } else if (bsp[i] == "..") {
                bskip++;
                adrop++;
            } else {
                break;
            }
        }
        if (adrop > 0) {
            if (alast == "\\" || alast == "/") {
                adrop++;
            }
            var asp = a.split(/[\\\/]/);
            var aspn = asp.length;
            asp.splice(aspn - adrop, adrop);
            a = asp.join("/");
        }
        bsp.splice(0, bskip);
        b = bsp.join("/");
        bfirst = b[0] || "";
    }
    alast = a[a.length-1];
    if (alast == "\\" || alast == "/" || bfirst == "\\" || bfirst == "/") {
        var sep = "";
    } else {
        sep = "/";
    }
    return a + sep + b;
}

function file_readable (pathname) {
    return (Ds.FileAccess(pathname, 4) == 0);
}

function file_read_json (pathname, notfound) {
    try {
        var content = Ds.FileReadAllText(pathname, "r");
        if (content == -1) {
            if (notfound === undefined) {
                throw new Error("Error reading file: "+path);
            } else {
                return notfound;
            }
        }
        return JSON.parse(content);
    } catch (e) {
        if (notfound === undefined) {
            throw e;
        } else {
            return notfound;
        }
    }
}



/*
 * Utils: Digistar object hierarchy
 */

function get_camera_parent (cam) {
    var excludeClasses = [Ds.GetClassID("compositionClass"),
                          Ds.GetClassID("textureCompositionClass")];
    var ps = Ds.GetObjectParentNames(cam);
    for (var i = 0, n = ps.length; i < n; ++i) {
        var p = ps[i];
        var pcl = Ds.GetObjectClassID(p);
        if (excludeClasses.indexOf(pcl) == -1) {
            return p;
        }
    }
    return null;
}


/*
 * Utils: Digistar
 */

function get_self_path () {
    var self = Ds.GetObjectAttr("js", "play");
    return drop_filename(self.pathName);
}


/*
 * Utils: Digistar script
 */

/**
 * format_script takes an array of lines and formats them as a Digistar
 * script suitable for passing to Ds.SendScriptCommands.  It prepends a
 * tab to every line (or puts it in between the timestamp and the
 * command), and appends a newline.
 */
function format_script (lines) {
    var script = "";
    var timestamp_re = RegExp('^(\\+?[\\d\\.]*)\\s*(.*)');
    for (var i = 0, n = lines.length; i < n; ++i) {
        var line = lines[i];
        var m = timestamp_re.exec(line);
        var ts = m[1];
        var command = m[2];
        script += ts + (command ? "\t" : "") + command + "\n";
    }
    return script;
}

function ScriptBuilder () {
    this.script = [];
}
ScriptBuilder.prototype = {
    constructor: ScriptBuilder,
    script: null,
    append: function (lines) {
        if (lines instanceof ScriptBuilder) {
            lines = lines.script;
        }
        this.script = this.script.concat(lines);
    },
    format: function () {
        return format_script(this.script);
    }
};
