
var media_collection;

function MediaPlugin (name) {
    this.name = name;
}
MediaPlugin.prototype = {
    constructor: MediaPlugin,
    toString: function () { return "#<MediaPlugin "+this.name+">"; },
    name: null
};


function MediaPack (owner, name, json_path) {
    this.name = name;
    this.json_path = json_path;
    var mediapack_spec = file_read_json(json_path);
    for (var media_name in mediapack_spec.media) {
        var media_spec = mediapack_spec.media[media_name];
        owner.ensure_plugin_loaded(media_spec.type);
    }
}
MediaPack.prototype = {
    constructor: MediaPack,
    toString: function () { return "#<MediaPack "+this.name+" ("+this.json_path+")>"; },
    name: null,
    json_path: null
};


function MediaCollection (media_root, media_plugins_path) {
    this.media_root = media_root;
    this.media_plugins_path = media_plugins_path;
    this.media_plugins = {};
    this.mediapacks = {};
}
MediaCollection.prototype = {
    constructor: MediaCollection,
    toString: function () { return "#<MediaCollection>"; },
    media_root: null,
    media_plugins_path: null,
    media_plugins: null,
    mediapacks: null,
    mediapack_add: function (mediapack_name) {
        var mediapack_path = join_paths(this.media_root, mediapack_name);
        var mediapack_json_path = join_paths(mediapack_path, mediapack_name + ".json");
        this.mediapacks[mediapack_name] =
            new MediaPack(this, mediapack_name, mediapack_json_path);
    },
    register_plugin: function (name, ctor) {
        this.media_plugins[name] = ctor;
    },
    ensure_plugin_loaded: function (plugin_name) {
        if (! (plugin_name in this.media_plugins)) {
            var plugin_path = join_paths(this.media_plugins_path, plugin_name + ".js");
            if (file_readable(plugin_path)) {
                include(Ds.ResolvePathName(plugin_path));
            }
        }
    },
    update_proximity: function (lat, lon) {
    }
};


function media_init (game_root_path) {
    var media_root = join_paths(game_root_path, "Media");
    var media_plugins_path = join_paths(game_root_path, "Scripts/MediaPlugins");
    media_collection = new MediaCollection(media_root, media_plugins_path);
}
