
/*
 * Utils: Math
 */

function degrees_to_radians (d) {
    return Math.PI * (d / 180.0);
}

function radians_to_degrees (r) {
    return r / Math.PI * 180.0;
}

function hms_to_sec (h, m, s) {
    return (3600 * h) + (60 * m) + s;
}

function sec_to_hms (sec) {
    var h = Math.floor(sec / 3600);
    sec = sec - (h * 3600);
    var m = Math.floor(sec / 60);
    var s = sec - (m * 60);
    return ""+h+":"+m+":"+s;
}

function sec_to_days (sec) {
    return (sec / (24 * 60 * 60));
}

/**
 * Rotate the given point about the origin by the given attitude.
 */
function rotate (pt, att) {
    var yaw = -att.h;
    var pitch = att.r;
    var roll = att.p;

    var cosa = Math.cos(yaw);
    var sina = Math.sin(yaw);

    var cosb = Math.cos(pitch);
    var sinb = Math.sin(pitch);

    var cosc = Math.cos(roll);
    var sinc = Math.sin(roll);

    var Axx = cosa*cosb;
    var Axy = cosa*sinb*sinc - sina*cosc;
    var Axz = cosa*sinb*cosc + sina*sinc;

    var Ayx = sina*cosb;
    var Ayy = sina*sinb*sinc + cosa*cosc;
    var Ayz = sina*sinb*cosc - cosa*sinc;

    var Azx = -sinb;
    var Azy = cosb*sinc;
    var Azz = cosb*cosc;

    var px = pt.x;
    var py = pt.y;
    var pz = pt.z;

    return { x: Axx*px + Axy*py + Axz*pz,
             y: Ayx*px + Ayy*py + Ayz*pz,
             z: Azx*px + Azy*py + Azz*pz };
}


function body_daylight_location (body) {
    var bodypos = Ds.GetObjectWorldPosition(body, false);
    var bodyatt = Ds.GetObjectAttr(body, "attitude", "CAR");
    var surface_pos = { // local position under the sun
        x: bodypos.x * 0.9999 - bodypos.x,
        y: bodypos.y * 0.9999 - bodypos.y,
        z: bodypos.z * 0.9999 - bodypos.z
    };
    var newpt = rotate(surface_pos, { h: -bodyatt.h, p: -bodyatt.p, r: -bodyatt.r });
    var sph = Ds.PosCartesianToSpherical(newpt.x, newpt.y, newpt.z);
    var loc = {
        lat: sph.el,
        lon: -sph.az + Math.PI * 0.5
    };
    return loc;
}

/**
 * get_lon_diff is how many degrees of longitude from src to dst, going
 * west.
 */
function get_lon_diff (src, dst) {
    if (src >= dst) {
        return src - dst;
    } else {
        return Math.abs(src + (360 - dst));
    }
}

function start (body, site_lon, day_length, time_direction) {
    var daylight_loc = body_daylight_location(body);
    var daylight_lon = radians_to_degrees(daylight_loc.lon);

    var lon_diff = get_lon_diff(daylight_lon, site_lon);

    // if site_lon is within 45 degrees of daylight_lon, do nothing.
    if (lon_diff < 45 || lon_diff > 315) {
        return;
    }

    if (time_direction == 0) {
        if (lon_diff <= 180) {
            time_direction = 1;
        } else {
            time_direction = -1;
        }
    }
    if (time_direction > 0) {
        var time_needed = (lon_diff / 360) * day_length;
        var dur = (lon_diff / 180) * 30;
    } else {
        time_needed = -(((360 - lon_diff) / 360) * day_length);
        dur = ((360 - lon_diff) / 180) * 30;
    }

    var scene_date = Ds.GetObjectAttr("scene", "date");
    Ds.SetObjectAttr("scene", "date", (scene_date + sec_to_days(time_needed)));
}
