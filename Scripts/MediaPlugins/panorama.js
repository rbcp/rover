
function MediaPluginPanorama () {
    MediaPlugin.call(this, "panorama");
}
MediaPluginPanorama.prototype = {
    constructor: MediaPluginPanorama,
    __proto__: MediaPlugin.prototype
};

media.register_plugin("panorama", MediaPluginPanorama);
