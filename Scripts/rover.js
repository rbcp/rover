
var rover_version = "0.0.1";

var game_controller;
var vehicles;
var mapinset_on = false;

var DIGISTAR_VERSION_GT_6_21_03 = false;

var refs = {
    system: {
        time: null
    }
};

include("./utils.js");
include("./media.js");

function refs_init () {
    for (var dsob in refs) {
        var attrs = refs[dsob];
        for (var attr in attrs) {
            attrs[attr] = Ds.NewObjectAttrRef(dsob, attr);
        }
    }
}

function compatibility_init () {
    var digistar_version = version_string_parse(Ds.GetObjectAttr("host", "version"));
    if (version_compare([6, 21, 03], digistar_version) > 0) {
        DIGISTAR_VERSION_GT_6_21_03 = true;
    }
}


/*
 * GameController
 */

function GameController () {
    this.button = { // buttons that we care about
        up: null,
        down: null,
        left: null,
        right: null,
        lbumper: null,
        rbumper: null,
        a: null,
        b: null,
        x: null,
        y: null
    };
}
GameController.prototype = {
    constructor: GameController,
    lx: 0,
    ly: 0,
    rx: 0,
    ry: 0,
    ltrigger: 0,
    rtrigger: 0,
    button: null, // sparse array of button states
    poll: function () {
        try {
            var axis = Ds.XBoxGetAxis(0, null);
            var buttons = Ds.XBoxGetButtons(0);
        } catch (e) {
            axis = {};
            buttons = 0;
        }
        this.lx = axis.lthumbx || 0;
        this.ly = axis.lthumby || 0;
        this.rx = axis.rthumbx || 0;
        this.ry = axis.rthumby || 0;
        this.ltrigger = axis.ltrigger || 0;
        this.rtrigger = axis.rtrigger || 0;
        this.button.up = buttons & 1;
        this.button.down = buttons & 2;
        this.button.left = buttons & 4;
        this.button.right = buttons & 8;
        this.button.lbumper = buttons & 256;
        this.button.rbumper = buttons & 512;
        this.button.a = buttons & 4096;
        this.button.b = buttons & 8192;
        this.button.x = buttons & 16384;
        this.button.y = buttons & 32768;
    }
};



/*
 * Vehicles
 */

function Vehicle (name, root_dsob, body, ground_offset) {
    this.name = name;
    this.root_dsob = root_dsob;
    this.body = body;
    this.ground_offset = ground_offset;
    this.cameras = {
        "default": {
            parent: root_dsob,
            position: { x: 0, y: 0, z: 0 },
            h: degrees_to_radians(0),
            p: degrees_to_radians(-45),
            dis: 10
        }
    };
    if (! DIGISTAR_VERSION_GT_6_21_03) {
        this.set_location = this._set_location_with_stringcommand;
    }
}
Vehicle.prototype = {
    constructor: Vehicle,
    toString: function () { return "#<Vehicle "+this.name+">"; },
    name: null,
    root_dsob: null,
    body: null,
    lat: 0,
    lon: 0,
    heading: 0,
    altitude: 0,
    ground_offset: 0, // Z for model to appear on the ground
    cameras: null,

    /**
     * set_location for backward compatibility D6 <= 6.21.03
     */
    _set_location_with_stringcommand: function (lat, lon, heading, altitude) {
        this.lat = lat;
        this.lon = lon;
        this.heading = heading;
        this.altitude = altitude;
        var z = this.ground_offset + this.altitude;
        var cmd = this.root_dsob + " location "+this.body+" "+
            radians_to_degrees(lat)+" "+radians_to_degrees(lon)+
            " ground "+z+" "+radians_to_degrees(heading)+
            " duration 0.1";
        Ds.SendStringCommand(cmd);
    },
    set_location: function (lat, lon, heading, altitude) {
        this.lat = lat;
        this.lon = lon;
        this.heading = heading;
        this.altitude = altitude;
        var location = {
            latitude: radians_to_degrees(lat),
            longitude: radians_to_degrees(lon),
            altitude: this.ground_offset + this.altitude,
            azimuth: radians_to_degrees(heading),
            ground: true
        };
        Ds.SetObjectLocation(this.root_dsob, this.body, location, 0, { total: 0.1 });
    },
    set_camera: function (camera_name) {
        var cam = this.cameras[camera_name];
        var current_parent = get_camera_parent("eye");
        if (current_parent != cam.parent) {
            if (current_parent) {
                Ds.RemoveObjectChild(current_parent, "eye");
            }
            Ds.AddObjectChild(cam.parent, "eye");
        }
        Ds.SetObjectAttr("eye", "standardSpace", false);
        Ds.SetObjectAttr("eye", "position", cam.position);
        Ds.SetObjectAttr("eye", "attitude", { h: cam.h, p: cam.p, r: 0 });
        Ds.SetObjectAttr("eye", "offset", { x: 0, y: -cam.dis, z: 0 });
    },
    update_camera_position: function (game_controller) {
        var cam = this.cameras["default"];
        var hchange = game_controller.rx * -1e-2;
        var pchange = game_controller.ry * -1e-2;
        var dis = ((game_controller.button.lbumper && 1) +
                   (game_controller.button.rbumper && -1)) * 0.1;
        if (hchange != 0 || pchange != 0) {
            cam.h = (cam.h + hchange) % tau;
            cam.p = (cam.p + pchange);
            cam.p = Math.min(Math.max(cam.p, -tau4), -tau24); // limit 15deg to 90deg
            Ds.SetObjectAttr("eye", "attitude", { h: cam.h, p: cam.p, r: 0 },
                             { total: 0.1 });
        }
        if (dis != 0) {
            cam.dis = Math.max(0, cam.dis + dis);
            Ds.SetObjectAttr("eye", "offset", { x: 0, y: -cam.dis, z: 0 },
                             { total: 0.1 });
        }
    },
    drive: function () {
        throw new Error("Subclasses of Vehicle must define method 'drive'.");
    }
};


function VehiclePerseverance (root_dsob, ground_offset) {
    Vehicle.call(this, "Perseverance", root_dsob, "Mars", ground_offset);
    Ds.SetObjectAttr(root_dsob+"_frontcam_frontarm", "attitude", { h: 0, p: tau4, r: 0 });
}
VehiclePerseverance.prototype = {
    constructor: VehiclePerseverance,
    __proto__: Vehicle.prototype,
    drive: function () {
        var hchange = game_controller.lx * 1e-2;
        var dis = game_controller.ly * 1e-7;
        if (hchange != 0 || dis != 0) {
            var new_pt = travel_spherical({ el: this.lat,
                                            az: this.lon,
                                            dis: 1 },
                                          this.heading + hchange,
                                          dis);
            this.set_location(new_pt.el, new_pt.az, this.heading + hchange, 0);
        }
    }
};

function VehicleIngenuity (root_dsob, ground_offset) {
    Vehicle.call(this, "Ingenuity", root_dsob, "Mars", ground_offset);

}
VehicleIngenuity.prototype = {
    constructor: VehicleIngenuity,
    __proto__: Vehicle.prototype,
    blades_spinning: false,
    blades_on: function () {
        var blade1 = "mars2020_ingenuity_blade01";
        var blade2 = "mars2020_ingenuity_blade02";
        Ds.SetObjectAttr(blade1, "attitude", { h: -40*tau, p: 0, r: 0 }, { total: 5, rate: true });
        Ds.SetObjectAttr(blade2, "attitude", { h: 40*tau, p: 0, r: 0 }, { total: 5, rate: true });
        this.blades_spinning = true;
    },
    blades_off: function () {
        var blade1 = "mars2020_ingenuity_blade01";
        var blade2 = "mars2020_ingenuity_blade02";
        Ds.SetObjectAttr(blade1, "attitude", { h: 0, p: 0, r: 0 }, { total: 5, rate: true });
        Ds.SetObjectAttr(blade2, "attitude", { h: 0, p: 0, r: 0 }, { total: 5, rate: true });
        this.blades_spinning = false;
    },
    drive: function () {
        if (this.blades_spinning) {
            var achange = game_controller.rtrigger - game_controller.ltrigger;
            var hchange = game_controller.lx * 1e-2;
            var dis = game_controller.ly * 5e-7;
            if (achange !=0 || hchange != 0 || dis != 0) {
                var new_alt = Math.max(0, this.altitude + achange);
                if (this.altitude > 0) {
                    var new_pt = travel_spherical({ el: this.lat,
                                                    az: this.lon,
                                                    dis: 1 },
                                                  this.heading + hchange,
                                                  dis);
                    if (new_alt == 0) {
                        this.blades_off();
                    }
                } else {
                    hchange = 0;
                    new_pt = { el: this.lat, az: this.lon };
                }
                this.set_location(new_pt.el, new_pt.az, this.heading + hchange, new_alt);
            }
        } else if (game_controller.button.a) {
            this.blades_on();
        }
    }
};


function Vehicles () {
    this.collection = {};
}
Vehicles.prototype = {
    constructor: Vehicles,
    toString: function () { return "#<Vehicles>"; },
    collection: null,
    _current: null,
    current: function () {
        return this.collection[this._current];
    },
    add: function (v) {
        this.collection[v.name] = v;
        if (! this._current) {
            this._current = v.name;
        }
    },
    set_current: function (name) {
        this._current = name;
    },
    get: function (name) {
        return this.collection[name] || null;
    }
};


/*
 * Callbacks
 */

function callback_timer_media_proximity (now) {
    var vehicle = vehicles.current();
    media_collection.update_proximity(vehicle.lat, vehicle.lon);
}

function callback_timer_drive (now) {
    game_controller.poll();
    var vehicle = vehicles.current();
    vehicle.drive(game_controller);
    vehicle.update_camera_position(game_controller);
    if (mapinset_on) {
        var att_h = (-vehicle.lon) % tau;
        var att_p = (-(Math.PI/2 + vehicle.lat)) % tau;
        Ds.SetObjectAttr('rover_mapinset_cam', 'attitude',
                         { h:att_h, p:att_p, r:0 });
    }
}

function callback_set_vehicle (message_cmd, vehicle_name) {
    var v = vehicles.get(vehicle_name);
    if (v == null) {
        print("Vehicle "+vehicle_name+" is not currently active.");
    } else {
        vehicles.set_current(vehicle_name);
        vehicles.current().set_camera("default");
    }
}

//XXX a vehicle-specific callback - can this be generalized to send a
//    message to a vehicle?
function callback_perseverance_deploy_ingenuity () {
    print("callback_perseverance_deploy_ingenuity");
    var v = vehicles.current();
    var ingenuity_root = "mars2020_ingenuity_root";
    var ingenuity_model = "mars2020_ingenuity";
    var script = new ScriptBuilder();
    var lat = radians_to_degrees(v.lat);
    var lon = radians_to_degrees(v.lon);
    var heading = radians_to_degrees(v.heading);
    script.append([
        ingenuity_model + " intensity 100",
        ingenuity_root + " location Mars "+lat+" "+lon+" ground 0.35 "+heading+
            " duration 0.5"
    ]);
    Ds.SendScriptCommands("rover-perseverance-deploy-ingenuity",
                          script.format());
    vehicles.add(new VehicleIngenuity("mars2020_ingenuity_root", 0.35));
    vehicles.get("Ingenuity").set_location(v.lat, v.lon, v.heading, 0);
}

function callback_mediapack_add (message_cmd, mediapack_name) {
    try {
        media_collection.mediapack_add(mediapack_name);
    } catch (e) {
        print("Error: failed to add MediaPack " + mediapack_name);
    }
}

function callback_mapinset_on () {
    mapinset_on = true;
    var vehicle = vehicles.current();
    var lat = radians_to_degrees(vehicle.lat);
    var lon = radians_to_degrees(vehicle.lon);
    var att_h = -lon;
    var att_p = -(90 + lat);
    var script = new ScriptBuilder();
    script.append([
        "rover_mapinset_cam attitude "+att_h+" "+att_p+" 0",
	"rover_mapinset intensity 100 duration 2"
    ]);
    Ds.SendScriptCommands("rover-mapinset-on", script.format());
}


/*
 * Main
 */

function EventRouter () {
    this.attribute_handlers = {};
    this.command_handlers = {};
    this.message_handlers = {};
    this.timer_handlers = [];
}
EventRouter.prototype = {
    constructor: EventRouter,
    toString: function () { return "#<EventRouter>"; },
    attribute_handlers: null,
    command_handlers: null,
    message_handlers: null,
    timer_handlers: null,
    ntimer_handlers: 0,
    handle_messages: false,
    running: false,
    register_attribute: function (dsob, attr, handler) {
        this.attribute_handlers[dsob+"."+attr] = handler;
        Ds.AddObjectAttrEvent(dsob, attr);
    },
    register_command: function (dsob, command, handler) {
        this.command_handlers[dsob+"."+command] = handler;
        Ds.AddObjectCommandEvent(dsob, command);
    },
    register_message: function (message, handler) {
        this.message_handlers[message] = handler;
        if (! this.handle_messages) {
            this.handle_messages = true;
            Ds.SetMessageEvent();
        }
    },
    register_timer_handler: function (interval, handler) {
        this.timer_handlers.push({
            interval: interval,
            handler: handler,
            lastcall: Ds.GetObjectAttrUsingRef(refs.system.time)
        });
        this.ntimer_handlers = this.timer_handlers.length;
        var g = this.timer_handlers[0].interval;
        for (var i = 1; i < this.ntimer_handlers; ++i) {
            g = gcd(g, this.timer_handlers[i].interval, 0.01);
        }
        Ds.SetTimerEvent(Math.max(0.05, g), "system");
    },
    message_parse: function (event) {
        var re = /([\S]+)\s*(.*)/;
        var m = re.exec(event);
        if (m) {
            return [m[1], m[2]];
        }
    },
    handle_event: function (event) {
        switch (typeof event) {
        case "DsObjectAttrRef":
            var dsob = Ds.GetAttrRefObjectName(event);
            var attr = Ds.GetAttrRefAttrName(event);
            var handler = this.attribute_handlers[dsob+"."+attr];
            handler(dsob, attr);
            break;
        case "DsObjectCommandRef":
            var dsob = Ds.GetCommandRefObjectName(event);
            var command = Ds.GetCommandRefCommandName(event);
            var handler = this.command_handlers[dsob+"."+command];
            handler(dsob, command);
            break;
        case "number": // timer interval
            var now = Ds.GetObjectAttrUsingRef(refs.system.time);
            for (var i = 0; i < this.ntimer_handlers; ++i) {
                var timer = this.timer_handlers[i];
                if (now >= timer.lastcall + timer.interval) {
                    timer.lastcall = now;
                    timer.handler(now);
                }
            }
            break;
        case "string": // control message
            var sp = this.message_parse(event);
            var cmd = sp[0];
            var handler = this.message_handlers[cmd];
            if (handler) {
                handler.apply(this, sp);
            }
            break;
        }
    },
    run: function () {
        this.running = true;
        while (this.running) {
            var event = Ds.WaitForEvent();
            if (typeof event == "object") {
                for (var i = 0, n = event.length; i < n; ++i) {
                    this.handle_event(event[i]);
                }
            } else {
                this.handle_event(event);
            }
        }
    },
    quit: function () {
        this.running = false;
    }
};


function start (rover_model) {
    print("Rover version "+rover_version+" starting");
    compatibility_init();

    var self_path = get_self_path();
    var game_root_path = drop_path_component(self_path);

    refs_init();

    var router = new EventRouter();

    router.register_message("set_vehicle", callback_set_vehicle);
    router.register_message("perseverance_deploy_ingenuity",
                            callback_perseverance_deploy_ingenuity);
    router.register_message("mediapack_add", callback_mediapack_add);
    router.register_message("mapinset_on", callback_mapinset_on);
    router.register_message("fadestopreset", function () { router.quit(); });
    router.register_message("quit", function () { router.quit(); });
    router.register_timer_handler(0.05, callback_timer_drive);
    router.register_timer_handler(5, callback_timer_media_proximity);

    media_init(game_root_path);

    game_controller = new GameController();
    vehicles = new Vehicles();
    vehicles.add(new VehiclePerseverance(rover_model, 1));
    vehicles.get("Perseverance").set_location(degrees_to_radians(18.44467),
                                              degrees_to_radians(77.45081),
                                              degrees_to_radians(200),
                                              0);
    vehicles.get("Perseverance").set_camera("default");

    router.run();
}
